import React , {Component} from 'react';
import { connect } from "react-redux";

export default ChildComponent => {
    class ComposedComponent extends Component {

        componentDidMount(){
            this.shouldNavigateAway();
          }
        
          componentDidUpdate(){
            this.shouldNavigateAway();
          }
        
          shouldNavigateAway(){
            if(!this.props.auth){
              console.log('No Auth HOC, Signing out')
            //   this.props.history.push('/login-page');
            }
          }
        

        render() {
            return <ChildComponent {...this.props} />
        }
    }

    function mapStateToProps (state) {
        return {auth:state.user};
      }

    return connect(mapStateToProps)(ComposedComponent);
}