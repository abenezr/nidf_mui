import {createSlice} from '@reduxjs/toolkit';
import api from '../../services/apiService';

const architectsSlice = createSlice({
    name: 'architects',
    initialState: {
        architects : null
    },
    reducers: {
        addArchitectsSuccess: (state, action) => {
            state.architects=action.payload
        }
    }
})

export default architectsSlice.reducer;

const {addArchitectsSuccess} = architectsSlice.actions;

export const loadArchitects = (archValues) => async dispatch => {
    try {
        const res = await api.get('/architects');
        console.log("architects Results",res.data)
        dispatch(addArchitectsSuccess(res.data))
    } catch (e) {
        return console.log(e.message)
    }
}