import {createSlice} from '@reduxjs/toolkit';
import api from '../../services/apiService';

const architectDesignsSlice = createSlice({
    name: 'architectDesigns',
    initialState: {
        architectDesigns : null
    },
    reducers: {
        loadArchitectDesignsSuccess: (state, action) => {
            state.architectDesigns=action.payload
        }
    }
})

export default architectDesignsSlice.reducer;

const {loadArchitectDesignsSuccess} = architectDesignsSlice.actions;

export const loadArchitectDesigns = (architectId) => async dispatch => {
    try {
        const res = await api.get('/architects?architect_id='+architectId);
        console.log("architects Designs",res.data)
        dispatch(loadArchitectDesignsSuccess(res.data))
    } catch (e) {
        return console.log(e.message)
    }
}