import {createSlice} from '@reduxjs/toolkit';
import api from '../../services/apiService';

const architectDetailsSlice = createSlice({
    name: 'architectDetails',
    initialState: {
        architectDetails : null
    },
    reducers: {
        loadArchitectDetailsSuccess: (state, action) => {
            state.architectDetails=action.payload
        }
    }
})

export default architectDetailsSlice.reducer;

const {loadArchitectDetailsSuccess} = architectDetailsSlice.actions;

export const loadArchitectDetails = (architectId) => async dispatch => {
    try {
        const res = await api.get('/architects?id='+architectId);
        console.log("architects Details",res.data)
        dispatch(loadArchitectDetailsSuccess(res.data))
    } catch (e) {
        return console.log(e.message)
    }
}