import {createSlice} from '@reduxjs/toolkit';
import api from '../../services/apiService';

const architectDesignDetailsSlice = createSlice({
    name: 'architectDesignDetails',
    initialState: {
        architectDesigns : null
    },
    reducers: {
        loadArchitectDesignDetailsSuccess: (state, action) => {
            state.architectDesignDetails=action.payload
        }
    }
})

export default architectDesignDetailsSlice.reducer;

const {loadArchitectDesignDetailsSuccess} = architectDesignDetailsSlice.actions;

export const loadArchitectDesignDetails = (architectId) => async dispatch => {
    try {
        const res = await api.get('/architects?architect_id='+architectId);
        console.log("architects Designs",res.data)
        dispatch(loadArchitectDesignDetailsSuccess(res.data))
    } catch (e) {
        return console.log(e.message)
    }
}