import {createSlice} from '@reduxjs/toolkit';
import api from '../../services/apiService';

const providersSlice = createSlice({
    name: 'providers',
    initialState: {
        providers : null
    },
    reducers: {
        addProvidersSuccess: (state, action) => {
            state.providers=action.payload
        }
    }
})

export default providersSlice.reducer;

const {addProvidersSuccess} = providersSlice.actions;

export const loadProviders = (archValues) => async dispatch => {
    try {
        const res = await api.get('/providers');
        console.log("providers ",res.data)
        dispatch(addProvidersSuccess(res.data))
    } catch (e) {
        return console.log(e.message)
    }
}