import {createSlice} from '@reduxjs/toolkit';
import api from '../../services/apiService';

const projectSlice = createSlice({
    name: 'project',
    initialState: {
        project : null
    },
    reducers: {
        createProjectSuccess: (state, action) => {
            state.project=action.payload
        }
    }
})

export default projectSlice.reducer;

const {createProjectSuccess} =projectSlice.actions;

export const createProject = (values,callback) => async dispatch => {
    try {
        const res = await api.post('/projects', values);
        dispatch(createProjectSuccess(values))
        callback();
    } catch (e) {
        return console.log(e.message)
    }
}

