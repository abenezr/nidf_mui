import {createSlice} from '@reduxjs/toolkit';
import api from '../../services/apiService';

const architectResultsSlice = createSlice({
    name: 'architectResults',
    initialState: {
        architectResults : null
    },
    reducers: {
        loadArchitectsSuccess: (state, action) => {
            state.architectResults=action.payload
        }
    }
})

export default architectResultsSlice.reducer;

const {loadArchitectsSuccess} = architectResultsSlice.actions;

export const loadArchitectResults = (archValues) => async dispatch => {
    try {
        const res = await api.get('/architects');
        console.log("architects Results",res.data)
        dispatch(loadArchitectsSuccess(res.data))
    } catch (e) {
        return console.log(e.message)
    }
}