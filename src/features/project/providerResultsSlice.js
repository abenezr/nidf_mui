import {createSlice} from '@reduxjs/toolkit';
import api from '../../services/apiService';

const providerResultsSlice = createSlice({
    name: 'providerResults',
    initialState: {
        providerResults : null
    },
    reducers: {
        loadProvidersSuccess: (state, action) => {
            state.providerResults=action.payload
        }
    }
})

export default providerResultsSlice.reducer;

const {loadProvidersSuccess} = providerResultsSlice.actions;

export const loadProviderResults = (archValues) => async dispatch => {
    try {
        const res = await api.get('/providers');
        console.log("providers Results",res.data)
        dispatch(loadProvidersSuccess(res.data))
    } catch (e) {
        return console.log(e.message)
    }
}