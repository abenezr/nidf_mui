import {createSlice} from '@reduxjs/toolkit';
import api from '../../services/apiService';

const contractorResultsSlice = createSlice({
    name: 'contractorResults',
    initialState: {
        contractorResults : null
    },
    reducers: {
        loadContractorsSuccess: (state, action) => {
            state.contractorResults=action.payload
        }
    }
})

export default contractorResultsSlice.reducer;

const {loadContractorsSuccess} = contractorResultsSlice.actions;

export const loadContractorResults = (archValues) => async dispatch => {
    try {
        const res = await api.get('/contractors');
        console.log("contractors Results",res.data)
        dispatch(loadContractorsSuccess(res.data))
    } catch (e) {
        return console.log(e.message)
    }
}