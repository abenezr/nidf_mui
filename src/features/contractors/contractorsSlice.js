import {createSlice} from '@reduxjs/toolkit';
import api from '../../services/apiService';

const contractorsSlice = createSlice({
    name: 'contractors',
    initialState: {
        contractors : null
    },
    reducers: {
        addContractorsSuccess: (state, action) => {
            state.contractors=action.payload
        }
    }
})

export default contractorsSlice.reducer;

const {addContractorsSuccess} = contractorsSlice.actions;

export const loadContractors = (archValues) => async dispatch => {
    try {
        const res = await api.get('/contractors');
        console.log("contractors Results",res.data)
        dispatch(addContractorsSuccess(res.data))
    } catch (e) {
        return console.log(e.message)
    }
}