import { createSlice } from "@reduxjs/toolkit";
import api from "../../services/apiService";

const initialUser = localStorage.getItem('user')
  ? JSON.parse(localStorage.getItem('user')) 
  : null


const userSlice = createSlice({
    name: 'user',
    initialState: {
        user: initialUser,
        errorMessage:null
    },
    reducers: {
        loginSuccess: (state,action) => {
            state.user = action.payload;
            localStorage.setItem('user', JSON.stringify(action.payload))
        },
        signUpSuccess: (state,action) => {
            state.user = action.payload;
            localStorage.setItem('user', JSON.stringify(action.payload))
        },
        logoutSuccess: (state,action) => {
            state.user = null;
            localStorage.removeItem('user');
            localStorage.removeItem('token');
        },
        loadUserDataSuccess:(state,action) => {
            state.user=action.payload;
            localStorage.setItem('user',JSON.stringify(action.payload))
        },
        userFailed:(state, action) => {
            state.errorMessage=action.payload
        }
    }
})

export default userSlice.reducer;

const { loginSuccess,signUpSuccess, logoutSuccess, loadUserDataSuccess, userFailed} = userSlice.actions;

export const login = (values,callback) => async dispatch => {
    try {        
        const res = await api.post('/login', values);
       dispatch(fetchUserData(values.email)) 
        callback();
        localStorage.setItem('token', res.data.accessToken)
    } catch (e) {
        dispatch(userFailed("Invalid Login"));
        return console.error(e.message);
    }
}
export const signUp = (values,callback) => async dispatch => {
    try {
        const res = await api.post('/register', values);
        dispatch(signUpSuccess(values));
        
            callback();
   
        localStorage.setItem('token', res.data.accessToken)
    } catch (e) {
        return console.error(e.message);
    }
}

export const logout = () => dispatch =>{
    try {
        // callback();
        return dispatch(logoutSuccess());

    } catch (e) {
        return console.error(e.message)
    }
}

export const fetchUserData = (email) => async dispatch => {
    try {
        const res = await api.get('/users?email='+email)
        return dispatch(loadUserDataSuccess(res.data[0]))
        
    } catch (e) {
        console.log(e.message)
    }
}