import Header from 'components/Header/Header';
import HeaderLinks from 'components/Header/HeaderLinks';
import React, { useEffect } from 'react';
import RenderContractorsList from 'views/contractors/ContractorListPage/sections/RenderContractorsList';


import { loadContractors } from "features/contractors/contractorsSlice";
import { useDispatch, useSelector } from 'react-redux';

 const ContractorsPage = () => {
    
    const dispatch = useDispatch();

     useEffect(() => {
         dispatch(loadContractors());
     })
    return(
        <div>
<Header
        color="transparent"
        brand="Nidf"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 200,
          color: "white"
        }}
        // {...rest}
      />

      <RenderContractorsList
        // contractors={contractors.contractors}
      />
        </div>
        
    )
}


export default ContractorsPage;