import Header from "components/Header/Header";
import HeaderLinks from "components/Header/HeaderLinks";
import Parallax from "components/Parallax/Parallax";
import React, { useEffect } from "react";
import { loadArchitectResults } from "features/project/architectResultsSlice";
import { loadContractorResults } from "features/project/contractorResultsSlice";
import { loadProviderResults } from "features/project/providerResultsSlice";

import { useDispatch, useSelector } from "react-redux";

import ArchitectResultsListTab from "views/ProjectResultPage/Sections/ArchitectResultsListTab";
import ProviderResultsListTab from "views/ProjectResultPage/Sections/ProviderResultsTab";
import ContractorResultsListTab from "views/ProjectResultPage/Sections/ContractorResultsListTab";


export const RenderArchitects = () => {
    const architectResults = useSelector(state => state.architectResults)
    console.log("Archts",architectResults.architectResults);
    if(architectResults.architectResults){
        return (

       <ArchitectResultsListTab 
       title="Architects List"
       architects= {architectResults.architectResults}
/>
    );
    }
    else {
        return(
            null
        )
    }
}

export const RenderProviders = () => {
    const providerResults = useSelector(state => state.providerResults)
    if(providerResults.providerResults){
        return (
            <ProviderResultsListTab 
       title="Material Providers List"
       providers= {providerResults.providerResults}
/>
            );
    }
    else {
        return(
            null
        )
    }
}

export const RenderContractors = () => {
    const contractorResults = useSelector(state => state.contractorResults)
    if(contractorResults.contractorResults){
        return (
            <ContractorResultsListTab 
       title="Contractors List"
       contractors= {contractorResults.contractorResults}
/>
            );
    }
    else {
        return(
            null
        )
    }
}

const ProjectResultPage = (props) => {

    const dispatch = useDispatch();


    // const architectsResults = useSelector(state => state.architectsResults)

    useEffect(() => {
            dispatch(loadArchitectResults())
            dispatch(loadContractorResults())
            dispatch(loadProviderResults())
    })

    return(
        <div>
            <Header
        color="dark"
        brand="Nidf"
        rightLinks={<HeaderLinks />}
        // fixed
        changeColorOnScroll={{
          height: 200,
          color: "white"
        }}
        // {...rest}
      />
     <div></div>
      {/* <Parallax style={{height:100,backgroundColor:'#A9B0BA'}} small filter small /> */}
<div style={{height:'30%', backgroundColor:'blue'}} >
            <RenderArchitects />
        </div>
        <div style={{height:'30%', backgroundColor:'blue'}} >
            <RenderProviders />
        </div>
        <div style={{height:'30%', backgroundColor:'blue'}} >
            <RenderContractors />
        </div>
        
        </div>
        
    )
}

export default ProjectResultPage;