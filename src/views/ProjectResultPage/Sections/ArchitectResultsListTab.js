import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons
import Face from "@material-ui/icons/Face";
import Chat from "@material-ui/icons/Chat";
import Build from "@material-ui/icons/Build";
// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import CustomTabs from "components/CustomTabs/CustomTabs.js";

import styles from "assets/jss/material-kit-react/views/componentsSections/tabsStyle.js";
import { Link } from "react-router-dom";

import profile1 from "assets/img/faces/christian.jpg";
import profile2 from "assets/img/faces/avatar.jpg";
import profile4 from "assets/img/faces/card-profile1-square.jpg";
import profile5 from "assets/img/faces/card-profile2-square.jpg";
import profile7 from "assets/img/faces/card-profile4-square.jpg";
import profile8 from "assets/img/faces/card-profile6-square.jpg";
import profile10 from "assets/img/faces/kendall.jpg";
import profile11 from "assets/img/faces/marc.jpg";

const useStyles = makeStyles(styles);

const imageList =[profile1,profile2 , profile4, profile5,profile7,profile8,profile10,profile11 ]

const RenderList = (architects,classes) => {
    return(
            architects.map((item, index) => (
                <GridItem xs={12} sm={12} md={6}>
                  <h3>
                    <small>{item.name}</small>
                  </h3>
                  
                 
                <CustomTabs
                headerColor="primary"
                tabs={[
                  {
                    tabName: "Profile",
                    tabIcon: Face,
                    tabContent: (
                      <React.Fragment>
                      <GridItem xs={12} sm={2} md={2} component={Link} to={`/architect-page/${item.id}`}>
              <img
                src={imageList[index%8]}
                alt="..."
                className={classes.imgRounded + " " + classes.imgFluid}
              />
            </GridItem>
            
                      <p className={classes.textCenter}>
                        I think that’s a responsibility that I have, to push
                        possibilities, to show people, this is the level that
                        things could be at. So when you get something that has
                        the name Kanye West on it, it’s supposed to be pushing
                        the furthest possibilities. I will be the leader of a
                        company that ends up being worth billions of dollars,
                        because I got the answers. I understand culture. I am
                        the nucleus.
                      </p>
                        
                      </React.Fragment>
                    )
                  },
                  {
                    tabName: "Messages",
                    tabIcon: Chat,
                    tabContent: (
                      <p className={classes.textCenter}>
                        I think that’s a responsibility that I have, to push
                        possibilities, to show people, this is the level that
                        things could be at. I will be the leader of a company
                        that ends up being worth billions of dollars, because I
                        got the answers. I understand culture. I am the nucleus.
                        I think that’s a responsibility that I have, to push
                        possibilities, to show people, this is the level that
                        things could be at.
                      </p>
                    )
                  },
                  {
                    tabName: "Settings",
                    tabIcon: Build,
                    tabContent: (
                      <p className={classes.textCenter}>
                        think that’s a responsibility that I have, to push
                        possibilities, to show people, this is the level that
                        things could be at. So when you get something that has
                        the name Kanye West on it, it’s supposed to be pushing
                        the furthest possibilities. I will be the leader of a
                        company that ends up being worth billions of dollars,
                        because I got the answers. I understand culture. I am
                        the nucleus.
                      </p>
                    )
                  }
                ]}
              />
              </GridItem>


            ))
    )
       
}

export default function ArchitectResultsListTab(props) {
    const {title , architects}= props;
  const classes = useStyles();
  return (
    <div className={classes.section}>
      <div className={classes.container}>
        <div id="nav-tabs">
          <h3>{title}</h3>
          <GridContainer>
            {/* <GridItem xs={12} sm={12} md={6}>
              <h3>
                <small>{architects[0].name}</small>
              </h3> */}
              

            {RenderList(architects, classes)}

          </GridContainer>
        </div>
      </div>
    </div>
  );
}
