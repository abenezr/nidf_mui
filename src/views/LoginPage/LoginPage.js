import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import CustomInput from "components/CustomInput/CustomInput.js";

import { Field, reduxForm } from "redux-form";
import TextField from "@material-ui/core/TextField";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import EmailIcon from "@material-ui/icons/Email";

import styles from "assets/jss/material-kit-react/views/loginPage.js";

import image from "assets/img/landing-bg.jpg";

import { login, logout } from "features/auth/userSlice";

const useStyles = makeStyles(styles);

const renderTextField = ({
  label,
  input,
  type,
  endIcon,
  fullWidth,
  meta: { touched, invalid, error },
  ...custom
}) => (
  <TextField
    label={label}
    placeholder={label}
    error={touched && invalid}
    helperText={touched && error}
    fullWidth={fullWidth}
    type={type}
    InputProps={{
      endAdornment: 

        endIcon ? 
          <InputAdornment children position="end">
          {endIcon}
          </InputAdornment>
        : 
         null 

        
          ,
    }}
    {...input}
    {...custom}
  />
);


const renderSelect = ({
  input,
  label,
  fullWidth,
  meta: { touched, error },
  children,
  ...custom
}) => (
  <Select
    floatingLabelText={label}
    value={label}
    fullWidth={fullWidth}
    errorText={touched && error}
    {...input}
    onChange={(event, index, value) => input.onChange(value)}
    children={children}
    {...custom}
  />
)

let SignInForm = (props) => {
  
  const classes = useStyles();
  const { handleSubmit } = props;

  const errorMessage = useSelector(state => state.user.errorMessage)

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <Field
          name="email"
          component={renderTextField}
          label="Email"
          endIcon={<Email />}
          fullWidth={true}
        />
      </div>
      <div style={{ marginTop: 20 }}>
        <Field
          name="password"
          component={renderTextField}
          label="password"
          type="password"
          fullWidth={true}
        />
      </div>
       {
        errorMessage ? 
        <div style={{color:'red'}}>Invalid Login</div>
        :
        null
      } 
      
      <CardFooter className={classes.cardFooter}>
                  <Button type="submit" simple color="primary" size="lg">
                    Sign In
                  </Button>
                </CardFooter>
    </form>
  );
};

SignInForm = reduxForm({
  form: "signin",
})(SignInForm);

function LoginPage(props) {

  const submit = values => {
    dispatch(login(values,()=> props.history.push('landing-page')));
  }

  const dispatch = useDispatch();
  const { user } = useSelector((state) => state.user);

  const [cardAnimaton, setCardAnimation] = React.useState("cardHidden");
  setTimeout(function () {
    setCardAnimation("");
  }, 700);
  const classes = useStyles();
  const { ...rest } = props;
  return (
    <div>
      <Header
        absolute
        color="transparent"
        brand="NIDF"
        rightLinks={<HeaderLinks />}
        {...rest}
      />

      <div
        className={classes.pageHeader}
        style={{
          backgroundImage: "url(" + image + ")",
          backgroundSize: "cover",
          backgroundPosition: "top center",
        }}
      >
        <div className={classes.container}>
          <GridContainer justify="center">
            <GridItem xs={12} sm={12} md={4}>
              <Card className={classes[cardAnimaton]}>
                {/* <form className={classes.form}> */}
                <CardHeader color="primary" className={classes.cardHeader}>
                  <h4>Login</h4>
                  <div className={classes.socialLine}>
                    <Button
                      justIcon
                      href="#pablo"
                      target="_blank"
                      color="transparent"
                      onClick={(e) => e.preventDefault()}
                    >
                      <i className={"fab fa-twitter"} />
                    </Button>
                    <Button
                      justIcon
                      href="#pablo"
                      target="_blank"
                      color="transparent"
                      onClick={(e) => e.preventDefault()}
                    >
                      <i className={"fab fa-facebook"} />
                    </Button>
                    <Button
                      justIcon
                      href="#pablo"
                      target="_blank"
                      color="transparent"
                      onClick={(e) => e.preventDefault()}
                    >
                      <i className={"fab fa-google-plus-g"} />
                    </Button>
                  </div>
                </CardHeader>
                <p className={classes.divider}>Or Be Classical</p>
                <CardBody>
                  <SignInForm user={user} onSubmit = {submit} />
                </CardBody>
                
                {/* </form> */}
              </Card>
            </GridItem>
          </GridContainer>
        </div>
        <Footer whiteFont />
      </div>
    </div>
  );
}

export default LoginPage;
