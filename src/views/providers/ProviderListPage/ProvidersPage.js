import Header from 'components/Header/Header';
import HeaderLinks from 'components/Header/HeaderLinks';
import React, { useEffect } from 'react';
import RenderProvidersList from 'views/providers/ProviderListPage/sections/RenderProvidersList';


import { loadProviders } from "features/providers/providersSlice";
import { useDispatch, useSelector } from 'react-redux';

 const ProvidersPage = () => {
    
    const dispatch = useDispatch();

     useEffect(() => {
         dispatch(loadProviders());
     })
    return(
        <div>
<Header
        color="info"
        brand="Nidf"
        rightLinks={<HeaderLinks />}
        // fixed
        changeColorOnScroll={{
          height: 200,
          color: "white"
        }}
        // {...rest}
      />

      <RenderProvidersList
        // providers={providers.providers}
      />
        </div>
        
    )
}


export default ProvidersPage;