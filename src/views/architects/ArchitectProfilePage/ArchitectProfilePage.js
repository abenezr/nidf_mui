import Footer from 'components/Footer/Footer';
import Header from 'components/Header/Header';
import HeaderLinks from 'components/Header/HeaderLinks';
import Parallax from 'components/Parallax/Parallax';
import React, { useEffect } from 'react';

// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/material-kit-react/views/profilePage.js";
import ArchitectDescriptionSection from './sections/ArchitectDescriptionSection';
import ArchitectDesignsSection from './sections/ArchitectDesignsSection';
import { useDispatch } from 'react-redux';
import { loadArchitectDetails } from 'features/architects/architectDetailsSlice';
import { loadArchitectDesigns } from 'features/architects/architectDesignsSlice';

const useStyles = makeStyles(styles);

const ArchitectProfilePage = ({match}) => {

    const architectId= match.params.architectId
    const dispatch = useDispatch();
console.log('id is',architectId)
    useEffect(() => {
        
        dispatch(loadArchitectDetails(architectId));
        dispatch(loadArchitectDesigns(architectId));        
    })

    const classes = useStyles();
    return(
        <div>
        <Header
        color="transparent"
        brand="Nidf"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 200,
          color: "white"
        }}
        // {...rest}
      />

<Parallax small filter image={require("assets/img/profile-bg.jpg")} />
<div className={classNames(classes.main, classes.mainRaised)}>
<div className={classes.container}>
    <ArchitectDescriptionSection />
    <ArchitectDesignsSection />
</div>
</div>
          
            <Footer />
        </div>
    )
}
export default ArchitectProfilePage