import Header from 'components/Header/Header';
import HeaderLinks from 'components/Header/HeaderLinks';
import React, { useEffect } from 'react';
import RenderArchitectsList from 'views/architects/ArchitectListPage/sections/RenderArchitectsList';


import { loadArchitects } from "features/architects/architectsSlice";
import { useDispatch, useSelector } from 'react-redux';

 const ArchitectsPage = () => {
    
    const dispatch = useDispatch();

     useEffect(() => {
         dispatch(loadArchitects());
     })
    return(
        <div>
<Header
        color="dark"
        brand="Nidf"
        rightLinks={<HeaderLinks />}
        // fixed
        changeColorOnScroll={{
          height: 200,
          color: "white"
        }}
        // {...rest}
      />

      <RenderArchitectsList
        // architects={architects.architects}
      />
        </div>
        
    )
}


export default ArchitectsPage;