import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import classNames from "classnames";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Button from "components/CustomButtons/Button.js";

//Form Components
import { Field, reduxForm } from "redux-form";


import Radio from "@material-ui/core/Radio";

import { createProject } from "features/project/projectSlice";
import GridContainer from "components/Grid/GridContainer";
import { Grid } from "@material-ui/core";
import styles from "assets/jss/material-kit-react/views/createProjectPage.js";
import GridItem from "components/Grid/GridItem";
import Parallax from "components/Parallax/Parallax";

import { renderTextField,renderCheckbox, renderRadioButton, renderFromHelper, renderSelectField, renderSelect} from 'components/ReduxInput/ReduxInput';

const useStyles = makeStyles(styles);

const validate = (values) => {
  const errors = {};
  const requiredFields = ["archText", "firstName", "lastName"];
  requiredFields.forEach((field) => {
    if (!values[field]) {
      errors[field] = "Required";
    }
  });
  if (
    values.email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
  ) {
    errors.email = "Invalid email address";
  }
  return errors;
};



CreateProjectForm = reduxForm({
  form: "createProject",
});

let CreateProjectForm = (props) => {
  const { handleSubmit, pristine, reset, submitting, classes } = props;

  const [archSelected, setArchSelected] = useState(false);

  const onChecked = () => {
    setArchSelected(!archSelected);
    console.log(archSelected);
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div>
            <div className={classes.container}>
              <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={6}>
                  <div>
                    <h1 className={classes.title}>Form</h1>
                  </div>

                  <div style={{ marginBottom: 10, marginTop: 10 }}>
                    <Field
                      name="archCategory"
                      component={renderCheckbox}
                      onChecked={onChecked}
                      label="I want an Architect"
                      fullWidth={true}
                    ></Field>
                  </div>
                  {archSelected ? (
                    <div>
                      <div style={{ marginBottom: 10, marginTop: 10 }}>
                        <Field
                          name="archGraduating"
                          component={renderCheckbox}
                          label="Graduating"
                          fullWidth={true}
                        ></Field>
                      </div>
                      <div style={{ marginBottom: 10, marginTop: 10 }}>
                        <Field
                          name="archIntern"
                          component={renderCheckbox}
                          label="Intern"
                          fullWidth={true}
                        ></Field>
                      </div>
                      <div style={{ marginBottom: 10, marginTop: 10 }}>
                        <Field
                          name="archPracticing"
                          component={renderCheckbox}
                          label="Practicing"
                          fullWidth={true}
                        ></Field>
                      </div>
                      <div style={{ marginBottom: 10, marginTop: 10 }}>
                        <Field
                          name="archProfessional"
                          component={renderCheckbox}
                          label="Professional"
                          fullWidth={true}
                        ></Field>
                      </div>
                    </div>
                  ) : null}

                  <div style={{ marginBottom: 10, marginTop: 10 }}>
                    <Field
                      name="archText"
                      component={renderTextField}
                      label="arch Text"
                      fullWidth={true}
                    />
                  </div>
                  <div style={{ marginBottom: 10, marginTop: 10 }}>
                    <Field
                      name="fistName"
                      component={renderTextField}
                      label="First Name"
                      fullWidth={true}
                    />
                  </div>
                  <div style={{ marginBottom: 10, marginTop: 10 }}>
                    <Field
                      name="lastName"
                      component={renderTextField}
                      label="Last Name"
                      fullWidth={true}
                    />
                  </div>
                  <div style={{ marginBottom: 10, marginTop: 10 }}>
                    <Field name="sex" component={renderRadioButton}>
                      <Radio value="male" label="male" />
                      <Radio value="female" label="female" />
                    </Field>
                  </div>
                  <div style={{ marginBottom: 10, marginTop: 10 }}>
                    <Field
                      classes={classes}
                      name="favoriteColor"
                      component={renderSelectField}
                      label="Favorite Color"
                    >
                      <option value="" />
                      <option value={"ff0000"}>Red</option>
                      <option value={"00ff00"}>Green</option>
                      <option value={"0000ff"}>Blue</option>
                    </Field>
                  </div>
                  <Button type="submit" simple color="primary" size="lg">
                    Create Project
                  </Button>
                </GridItem>
              </GridContainer>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};

CreateProjectForm = reduxForm({
  form: "createProject",
})(CreateProjectForm);

const CreateProjectFormPage = (props) => {
  const classes = useStyles();

  const submit = (values) => {
    dispatch(
      createProject(values, () => props.history.push("project-results"))
    );
  };

  const dispatch = useDispatch();
  const { project } = useSelector((state) => state.project);

  const { ...rest } = props;
  return (
    <div>
      <Header
        color="transparent"
        brand="Nidf"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 200,
          color: "white",
        }}
        {...rest}
      />

      <Parallax
        height={20}
        small
        filter
        image={require("assets/img/landing-bg.jpg")}
      />

      <CreateProjectForm onSubmit={submit} classes={classes} />

      <Grid container>
        <Grid item>
          <div></div>
        </Grid>
      </Grid>
      <Footer />
    </div>
  );
};

export default CreateProjectFormPage;
