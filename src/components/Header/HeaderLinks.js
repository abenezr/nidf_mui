/*eslint-disable*/
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
// react components for routing our app without refresh
import { Link } from "react-router-dom";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";

// @material-ui/icons
import { Apps, CloudDownload } from "@material-ui/icons";

// core components
import CustomDropdown from "components/CustomDropdown/CustomDropdown.js";
import Button from "components/CustomButtons/Button.js";

import styles from "assets/jss/material-kit-react/components/headerLinksStyle.js";

import { login, logout } from "features/auth/userSlice";

const useStyles = makeStyles(styles);

const renderMYLinks = (user) => {
  // console.log(user.provider)
  if(user.provider) {
    return(<Link to="/" className={classes.dropdownLink}>
    My Provider Page
  </Link>
    )
  }
  else return(
    null
  )
}

const headerDropDowns = (user, classes,handleLogOut) => {

  let list = [<Link to="/login-page" className={classes.dropdownLink}>
  <Button
  onClick={handleLogOut}
  >
      Log Out
  </Button>
</Link>]
  if(user.provider){
    list = [<Link to="/" className={classes.dropdownLink}>
    My Provider Page
  </Link>, ...list ]
  }
  if(user.contractor){
    list = [<Link to="/" className={classes.dropdownLink}>
    My Contactor Page
  </Link>,...list ]
  }
  if(user.architect){
    list = [<Link to="/" className={classes.dropdownLink}>
    My Architect Page
  </Link>,...list ]
  }
  return list
  // (
  //   [
            
  //     <Link to="/" className={classes.dropdownLink}>
  //       My Architect Page
  //     </Link>,
      
  //   ,
  //   <Link to="/" className={classes.dropdownLink}>
  //   My Contractor Page
  // </Link>,
  
  //   ]
  // )
}


const renderListItem = (user,handleLogOut) => {

  const classes = useStyles();
  if(user){
    return(
      <ListItem className={classes.listItem}>
        <CustomDropdown
          noLiPadding
          buttonText={user.email}
          buttonProps={{
            className: classes.navLink,
            color: "transparent"
          }}
          
          dropdownList={
            headerDropDowns(user, classes,handleLogOut)
          }
        />
      </ListItem>
    )
    
  }
  else{
    return(
      <>
     <ListItem className={classes.listItem}>
        <Link to="/login-page" className={classes.dropdownLink}>
              Sign In
            </Link>

      </ListItem> 
      <ListItem className={classes.listItem}>
      <Link to="/signup-page" className={classes.dropdownLink}>
            Sign Up
          </Link>

    </ListItem> 
    </>
    )

  }
}

export default function HeaderLinks(props) {

 const dispatch = useDispatch();
//  props.history.push('/login-page')

  const handleLogOut = () => {
    dispatch(logout())
  }

  const { user } = useSelector((state) => state.user);

  const classes = useStyles();
  return (
    <List className={classes.list}>
      <ListItem className={classes.listItem}>
        <CustomDropdown
          noLiPadding
          buttonText="Services"
          buttonProps={{
            className: classes.navLink,
            color: "transparent"
          }}
          buttonIcon={Apps}
          dropdownList={[
            <Link to="/architects-list" className={classes.dropdownLink}>
              Architects
            </Link>,
            <Link to="/providers-list" className={classes.dropdownLink}>
            Providers
          </Link>,
          <Link to="/contractors-list" className={classes.dropdownLink}>
          Contractors
        </Link>
          ]}
        />
      </ListItem>
      
      <ListItem className={classes.listItem}>
        {/*<Tooltip title="Delete">
          <IconButton aria-label="Delete">
            <DeleteIcon />
          </IconButton>
        </Tooltip>*/}
        <Tooltip
          id="instagram-twitter"
          title="Follow us on twitter"
          placement={window.innerWidth > 959 ? "top" : "left"}
          classes={{ tooltip: classes.tooltip }}
        >
          <Button
            href=""
            target="_blank"
            color="transparent"
            className={classes.navLink}
          >
            <i className={classes.socialIcons + " fab fa-twitter"} />
          </Button>
        </Tooltip>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Tooltip
          id="instagram-facebook"
          title="Follow us on facebook"
          placement={window.innerWidth > 959 ? "top" : "left"}
          classes={{ tooltip: classes.tooltip }}
        >
          <Button
            color="transparent"
            href=""
            target="_blank"
            className={classes.navLink}
          >
            <i className={classes.socialIcons + " fab fa-facebook"} />
          </Button>
        </Tooltip>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Tooltip
          id="instagram-tooltip"
          title="Follow us on instagram"
          placement={window.innerWidth > 959 ? "top" : "left"}
          classes={{ tooltip: classes.tooltip }}
        >
          <Button
            color="transparent"
            href=""
            target="_blank"
            className={classes.navLink}
          >
            <i className={classes.socialIcons + " fab fa-instagram"} />
          </Button>
        </Tooltip>
      </ListItem>
      {renderListItem(user, handleLogOut)}
      
    </List>
  );
}
