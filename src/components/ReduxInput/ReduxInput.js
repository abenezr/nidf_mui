
import React from 'react';
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import Checkbox from "@material-ui/core/Checkbox";

import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";

import RadioGroup from "@material-ui/core/RadioGroup";

import Radio from "@material-ui/core/Radio";

 export const renderTextField = ({
    label,
    input,
    type,
    endIcon,
    fullWidth,
    meta: { touched, invalid, error },
    ...custom
  }) => (
    <TextField
      label={label}
      variant="outlined"
      placeholder={label}
      error={touched && invalid}
      helperText={touched && error}
      fullWidth={fullWidth}
      type={type}
      InputProps={{
        endAdornment: endIcon ? (
          <InputAdornment children position="end">
            {endIcon}
          </InputAdornment>
        ) : null,
      }}
      {...input}
      {...custom}
    />
  );
  

export const renderCheckbox = ({ input, label, onChecked }) => (
    <div>
      <FormControlLabel
        control={
          <Checkbox
            checked={input.value ? true : false}
            onChange={() => {
              input.onChange(!input.value);
              if (onChecked) {
                onChecked();
              }
            }}
          />
        }
        label={label}
      />
    </div>
  );
  
 export const renderRadioButton = ({ input, ...rest }) => (
    <FormControl>
      <RadioGroup {...input} {...rest}>
        <FormControlLabel value="female" control={<Radio />} label="Female" />
        <FormControlLabel value="male" control={<Radio />} label="Male" />
        <FormControlLabel value="other" control={<Radio />} label="Other" />
      </RadioGroup>
    </FormControl>
  );
  
  export const renderFromHelper = ({ touched, error }) => {
    if (!(touched && error)) {
      return;
    } else {
      return <FormHelperText>{touched && error}</FormHelperText>;
    }
  };
  
  export const renderSelectField = ({
    input,
    label,
    meta: { touched, error },
    children,
    ...custom
  }) => (
    <FormControl error={touched && error}>
      <InputLabel htmlFor="age-native-simple">Age</InputLabel>
      <Select
        native
        {...input}
        {...custom}
        inputProps={{
          name: "age",
          id: "age-native-simple",
        }}
      >
        {children}
      </Select>
      {renderFromHelper({ touched, error })}
    </FormControl>
  );
  
  export const renderSelect = ({
    input,
    label,
    fullWidth,
    meta: { touched, error },
    children,
    ...custom
  }) => (
    <Select
      floatingLabelText={label}
      value={label}
      fullWidth={fullWidth}
      errorText={touched && error}
      {...input}
      onChange={(_event, index, value) => input.onChange(value)}
      children={children}
      {...custom}
    />
  );