import { applyMiddleware, combineReducers, createStore } from '@reduxjs/toolkit';
import user from 'features/auth/userSlice';

import project from 'features/project/projectSlice';

import architectResults from 'features/project/architectResultsSlice';
import providerResults from 'features/project/providerResultsSlice';
import contractorResults from 'features/project/contractorResultsSlice';

import architects from 'features/architects/architectsSlice';
import contractors from 'features/contractors/contractorsSlice';
import providers from 'features/providers/providersSlice';

import architectDetails from 'features/architects/architectDetailsSlice';
import architectDesigns from 'features/architects/architectDesignsSlice';
import architectDesignDetails from 'features/architects/architectDesignDetailsSlice';

import thunk from 'redux-thunk';
import logger from 'redux-logger';

import { reducer as formReducer } from 'redux-form'

import { composeWithDevTools } from 'redux-devtools-extension';

const reducer = combineReducers({
    user,
    project,
    architectResults,
    providerResults,
    contractorResults,
    architects,
    contractors,
    providers,
    architectDetails,
    architectDesigns,
    architectDesignDetails,
    form:formReducer
})

const store = createStore(
  reducer,
  {},
  composeWithDevTools(applyMiddleware(thunk))
  
)

export default store;
