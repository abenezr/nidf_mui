import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Link } from "react-router-dom";
import { Provider } from "react-redux";

import store from './app/store'

import "assets/scss/material-kit-react.scss?v=1.9.0";

// pages for this product
import Components from "views/Components/Components.js";
import LandingPage from "views/LandingPage/LandingPage.js";
import ProfilePage from "views/ProfilePage/ProfilePage.js";
import LoginPage from "views/LoginPage/LoginPage.js";
import SignUpPage from "views/SignUpPage/SignUpPage.js";
import CreateProjectFormPage from "views/CreateProjectPage/CreateProjectFormPage";
import ProjectResultPage from "views/ProjectResultPage/ProjectResultPage";
import ArchitectListPage from 'views/architects/ArchitectListPage/ArchitectListPage';
import ProviderListPage from 'views/providers/ProviderListPage/ProvidersPage';
import ContractorListPage from 'views/contractors/ContractorListPage/ContractorListPage';

import ArchitectProfilePage from 'views/architects/ArchitectProfilePage/ArchitectProfilePage';
import DesignPage from "views/architects/DesignPage/DesignPage";

var hist = createBrowserHistory();

ReactDOM.render(
<Provider store={store}>
  <Router history={hist}>
    <Switch>
      <Route path="/landing-page" component={LandingPage} />
      <Route path="/profile-page" component={ProfilePage} />
      <Route path="/login-page" component={LoginPage} />
      <Route path="/signup-page" component={SignUpPage} />
      <Route path='/create-project' component={CreateProjectFormPage} />
      <Route path='/project-results' component={ProjectResultPage} />
      <Route path="/architects-list" component={ArchitectListPage} />
      <Route exact path="/architect-page/:architectId" component={ArchitectProfilePage} />
      <Route exact path='/architect-page/:architectId/:designId' component={DesignPage} />
      <Route path='/providers-list' component = {ProviderListPage} />
      <Route path='/contractors-list' component={ContractorListPage} />
      {/* <Route path='/architect-page' component={ArchitectProfilePage} /> */}
      
      <Route path='/providers-page' component={LandingPage} />
      <Route path='/contractors-page' component={LandingPage}/>

      <Route path="/components" component={Components} />

      <Route path="/" component={()=><Link to='/landing-page'>
        <div>Not Implemented, Go Back To Landing Page</div>
      </Link>} />
    </Switch>
  </Router>
  </Provider>,
  document.getElementById("root")
);
